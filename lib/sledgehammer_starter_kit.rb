# frozen_string_literal: true

require_relative 'ssk/version'

module Ssk
  class Error < StandardError; end
  # Your code goes here...
end
