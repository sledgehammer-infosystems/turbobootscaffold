# frozen_string_literal: true

require "rails/generators/rails/model/model_generator"

require_relative '../scaffold_controller/scaffold_controller_generator.rb'
require_relative '../scaffold_views/scaffold_views_generator.rb'

module Ssk
  module Generators
    class ScaffoldGenerator < Rails::Generators::NamedBase # :nodoc:
      source_root File.expand_path('templates', __dir__)

      class_option :resource_route, type: :boolean

      invoke Rails::Generators::ModelGenerator

      invoke Ssk::Generators::ScaffoldControllerGenerator
      invoke Ssk::Generators::ScaffoldViewsGenerator
    end
  end
end
