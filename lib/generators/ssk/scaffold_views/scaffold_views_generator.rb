# frozen_string_literal: true

# require "rails/generators/erb"
# require "rails/generators/resource_helpers"

module Ssk
  module Generators
    class ScaffoldViewsGenerator < Rails::Generators::NamedBase # :nodoc:
      include Rails::Generators::ResourceHelpers

      source_root File.expand_path('templates', __dir__)

      class_option :belongs_to, type: :string, banner: 'name of parent model', reuqired: false

      argument :attributes, type: :array, default: [], banner: 'field:type field:type'

      def create_root_folder
        empty_directory File.join('app/views', controller_file_path)
      end

      def copy_view_files
        available_views.each do |view|
          formats.each do |format|
            filename = filename_with_extensions(view, format)
            template filename, File.join('app/views', controller_file_path, filename)
          end
        end
      end

      private

      def available_views
        %w[index edit show new _form]
      end

      def formats
        [:html]
      end

      def handler
        :erb
      end

      def filename_with_extensions(name, file_format = format)
        [name, file_format, handler].compact.join('.')
      end

      def parent?
        options[:belongs_to].present?
      end

      def parent_name_singular
        options[:belongs_to]
      end

      def parent_name_plural
        parent_name_singular.pluralize if options[:belongs_to]
      end

      def link_target(t)
        if parent?
          "[@#{parent_name_singular}, #{t}]"
        else
          t
        end
      end

      def index_helper(type: nil) # :doc:
        [parent_name_singular, plural_route_name, ('index' if uncountable?), type].compact.join('_')
      end

      def show_helper(arg = "@#{singular_table_name}", type: :url) # :doc:
        "#{[parent_name_singular, singular_route_name,
            type].compact.join('_')}(#{[parent? ? "@#{parent_name_singular}" : nil, arg].compact.join(', ')})"
      end

      def edit_helper(...) # :doc:
        "edit_#{show_helper(...)}"
      end

      def new_helper(...) # :doc:
        "new_#{show_helper(...)}"
      end
    end
  end
end
