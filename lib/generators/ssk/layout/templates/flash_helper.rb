module FlashHelper
  def flash_to_class(type)
    bootstrap_type = case type
                     when 'notice'
                       'info'
                     else
                       type
                     end

    "alert-#{bootstrap_type}"
  end

  def flash_to_icon(type)
    bootstrap_icon = case type
                     when 'success'
                       'circle-check'
                     when 'warning', 'danger'
                       'triangle-exclamation'
                     else
                       'circle-info'
                     end

    "fa-#{bootstrap_icon}"
  end
end
