module IconHelper
  def icon(names = 'flag', original_options = {})
    options = original_options.deep_dup
    classes = ['fa']
    classes.concat Private.icon_names(names)
    classes.concat Array(options.delete(:class))
    text = options.delete(:text)
    right_icon = options.delete(:right)
    icon = content_tag(:i, nil, options.merge(class: classes))
    Private.icon_join(icon, text, right_icon)
  end

  def font_awesome_tag
    tag.script src: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/js/all.min.js', data: { mutateApproach: 'sync' }
  end

  module Private
    extend ActionView::Helpers::OutputSafetyHelper

    def self.icon_join(icon, text, reverse_order = false)
      return icon if text.blank?

      elements = [icon, ERB::Util.html_escape(text)]
      elements.reverse! if reverse_order
      safe_join(elements, ' ')
    end

    def self.icon_names(names = [])
      array_value(names).map { |n| "fa-#{n}" }
    end

    def self.array_value(value = [])
      value.is_a?(Array) ? value : value.to_s.split(/\s+/)
    end
  end
end
