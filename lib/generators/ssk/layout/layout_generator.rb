# frozen_string_literal: true

module Ssk
  module Generators
    class LayoutGenerator < Rails::Generators::Base
      source_root File.expand_path('templates', __dir__)

      argument :style, type: :string, banner: '[boring|navbar|grid|sidenav]' # , default: 'boring'

      def generate_layout_files
        template "#{style}.html.erb", File.join('app', 'views', 'layouts', 'application.html.erb')
        copy_file 'logo.svg', File.join('app', 'assets', 'images', 'logo.svg')
        return unless style == 'sidenav'

        copy_file '_sidebar.scss', File.join('app', 'assets', 'stylesheets', '_sidebar.scss')
        append_to_file File.join('app', 'assets', 'stylesheets', 'application.bootstrap.scss'), "@import 'sidebar';"
      end

      def helpers
        copy_file 'icon_helper.rb', File.join('app', 'helpers', 'icon_helper.rb')
        copy_file 'flash_helper.rb', File.join('app', 'helpers', 'flash_helper.rb')
      end

      def simple_form
        run 'bundle add simple_form'
        generate 'simple_form:install', '--bootstrap'
      end
    end
  end
end
