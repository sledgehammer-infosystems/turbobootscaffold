# frozen_string_literal: true

module Ssk
  module Generators
    class HelperGenerator < Rails::Generators::NamedBase # :nodoc:
      check_class_collision suffix: 'Helper'

      source_root File.expand_path('templates', __dir__)

      def create_helper_files
        template 'helper.rb.tt', File.join('app/helpers', class_path, "#{file_name}_helper.rb")
      end

      hook_for :test_framework

      private

      def file_name
        @file_name ||= super.sub(/_helper\z/i, '')
      end
    end
  end
end
