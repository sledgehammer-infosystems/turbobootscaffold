# frozen_string_literal: true

module Ssk
  module Generators
    class PoroGenerator < Rails::Generators::NamedBase
      source_root File.expand_path('templates', __dir__)

      argument :accessors, type: :array, banner: 'attr [attr]'
      class_option :comparable, type: :boolean, default: true, desc: 'Include Comparable & <=>'

      remove_class_option :skip_namespace
      remove_class_option :skip_collision_check

      check_class_collision

      def generate_object_file
        template 'model.rb', File.join('app', 'models', "#{file_name}.rb")
      end

      def generate_object_test_file
        template 'model_test.rb', File.join('test', 'lib', 'models', "#{file_name}_test.rb")
      end
    end
  end
end
