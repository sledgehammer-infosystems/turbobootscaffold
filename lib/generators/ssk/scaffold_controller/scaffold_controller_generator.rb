# frozen_string_literal: true

# require "rails/generators/resource_helpers"
# require_relative '../generator_helpers.rb'

module Ssk
  module Generators
    class ScaffoldControllerGenerator < Rails::Generators::NamedBase # :nodoc:
      include Rails::Generators::ResourceHelpers

      source_root File.expand_path('templates', __dir__)

      check_class_collision suffix: 'Controller'

      class_option :belongs_to, type: :string, banner: 'name of parent model', reuqired: false
      class_option :helper, type: :boolean
      class_option :orm, banner: 'NAME', type: :string, required: true,
                         desc: 'ORM to generate the controller for'

      class_option :skip_routes, type: :boolean, desc: "Don't add routes to config/routes.rb."

      argument :attributes, type: :array, default: [], banner: 'field:type field:type'

      def create_controller_files
        template (options[:belongs_to].present? ? 'parented_controller.rb.tt' : 'controller.rb.tt'),
                 File.join('app/controllers', controller_class_path, "#{controller_file_name}_controller.rb")
      end

      hook_for :resource_route, required: true do |route|
        invoke route unless options.skip_routes?
      end

      hook_for :test_framework, as: :scaffold

      # Invoke the helper using the controller name (pluralized)
      hook_for :helper, as: :scaffold do |invoked|
        invoke invoked, [controller_name]
      end

      private

      def permitted_params
        attachments, others = attributes_names.partition { |name| attachments?(name) }
        params = others.map { |name| ":#{name}" }
        params += attachments.map { |name| "#{name}: []" }
        params.join(', ')
      end

      def attachments?(name)
        attribute = attributes.find { |attr| attr.name == name }
        attribute&.attachments?
      end

      def parent_name_singular
        options[:belongs_to]
      end

      def parent_name_plural
        parent_name_singular.pluralize if options[:belongs_to]
      end
    end
  end
end
