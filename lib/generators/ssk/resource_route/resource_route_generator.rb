# frozen_string_literal: true

module Ssk
  module Generators
    class ResourceRouteGenerator < Rails::Generators::NamedBase # :nodoc:
      # Properly nests namespaces passed into a generator
      #
      #   $ bin/rails generate resource admin/users/products
      #
      # should give you
      #
      #   namespace :admin do
      #     namespace :users do
      #       resources :products
      #     end
      #   end
      
      class_option :belongs_to, type: :string, banner: 'name of parent model', reuqired: false

      def add_resource_route
        return if options[:actions].present?

        # TODO: this could stand to be... a lot better
        if parent?
          child_route = "\n\t\tresources :#{file_name.pluralize}"
          parent_route_pattern = /resources :#{parent_name_plural} do/
          if match_file('config/routes.rb', parent_route_pattern)
            inject_into_file 'config/routes.rb', child_route, after: parent_route_pattern, force: false
          else
            child_route = " do" + child_route + "\n\tend"
            parent_route_pattern = /resources :#{parent_name_plural}/
            if match_file('config/routes.rb', parent_route_pattern)
              inject_into_file 'config/routes.rb', child_route, after: parent_route_pattern, force: false
            end
          end
        else
          route "resources :#{file_name.pluralize}", namespace: regular_class_path
        end
      end

      private

        def parent?
          options[:belongs_to].present?
        end

        def parent_name_singular
          options[:belongs_to]
        end

        def parent_name_plural
          parent_name_singular.pluralize if options[:belongs_to]
        end
    end
  end
end
