# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)
require 'sledgehammer_starter_kit'

require 'minitest/autorun'
